const http = require("http");
const port = 8000;

// Mock Data
let items = [
	{
		name : "Iphone X",
		price: 30000,
		isActive: true
	},
	{
		name : "Samsung Galaxy s21",
		price: 51000,
		isActive: true
	},
	{
		name : "Razer Blackshark VX2",
		price: 2800,
		isActive: false
	}
]


const server = http.createServer((req, res) => {
	if(req.url === "/items" && req.method === "GET"){
		res.writeHead(200, {'Content-Type' : 'application/json'});
		res.end(JSON.stringify(items));
	}
	else if(req.url === "/items" && req.method === "POST"){
		let requestBody = "";

		req.on('data', (data) => {
			console.log(data.toString());
			requestBody += data;
		});


		req.on('end', () => {
			console.log(requestBody);

			requestBody = JSON.parse(requestBody);

			console.log(requestBody);

			let newItem = {
				name : requestBody.name,
				price : requestBody.price,
				isActive : requestBody.isActive
			};

			console.log(newItem);
			items.push(requestBody);
			console.log(items);


			res.writeHead(200, {'Content-Type' : 'application/json'});
			res.end(JSON.stringify(items));
			


		});
	}

	else if (req.url === "/items" && req.method === "DELETE"){
		items.pop();

		res.writeHead(200, {'Content-Type' : 'application/json'});
		res.end(JSON.stringify(items));
	}



	
});

server.listen(port);
console.log(`Server is running on localhost:${port}`);